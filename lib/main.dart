import 'package:flutter/material.dart';
import 'package:process_run/shell.dart';
import 'package:window_manager/window_manager.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Brightness Kontroller',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'BriKtl'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double _counter = 0;
  var shell = Shell();

  void _incrementCounter() {
    _counter += 0.1;
    setState(() {
      shell.run('xrandr --output HDMI-A-0 --brightness $_counter');
    });
  }

  void _decreementCounter() {
    if (_counter > 0) {
      _counter -= 0.1;
    }
    setState(() {
      shell.run('xrandr --output HDMI-A-0 --brightness $_counter');
    });
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await windowManager.setSize(Size(
        MediaQuery.of(context).size.width * 0.3,
        MediaQuery.of(context).size.height * 0.4,
      ));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(
          widget.title,
          style: Theme.of(context).textTheme.displaySmall,
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: (value) {
          switch (value) {
            case 0:
              _incrementCounter();
              break;
            case 1:
              _decreementCounter();
              break;
            default:
          }
        },
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.brightness_high),
            label: 'High',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.brightness_low),
            label: 'Low',
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Brightness:',
              style: Theme.of(context).textTheme.displaySmall,
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.displaySmall,
            ),
          ],
        ),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: showOptionsDialog,
            tooltip: 'Options',
            child: const Icon(Icons.tune),
          ),
        ],
      ),
    );
  }

  void showOptionsDialog() {
    
  }
}
